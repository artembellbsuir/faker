﻿using FakerConsole.GeneratorPlugins;
using System;

namespace ValueGenerators.Generators
{
    public class StringGenerator : IStringGenerator
    {
        //private static Random random;

        public dynamic Generate(Type type)
        {
            var random = new Random();
            byte[] bytesBuffer = new byte[8];
            random.NextBytes(bytesBuffer);

            return BitConverter.ToString(bytesBuffer);
        }

        public Type GetGeneratingType()
        {
            return typeof(string);
        }

        //public string GetTypeRandomValue()
        //{
        //    //var random = new Random();
        //    byte[] bytesBuffer = new byte[8];
        //    random.NextBytes(bytesBuffer);

        //    return BitConverter.ToString(bytesBuffer);
        //}
    }
}
