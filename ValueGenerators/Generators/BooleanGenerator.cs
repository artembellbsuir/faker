﻿using FakerConsole.GeneratorPlugins;
using System;

namespace ValueGenerators.Generators
{
    public class BooleanGenerator : IBooleanGenerator
    {
        public dynamic Generate(Type type)
        {
            var random = new Random();
            byte[] bytesBuffer = new byte[8];
            random.NextBytes(bytesBuffer);

            return BitConverter.ToBoolean(bytesBuffer, 7);
        }

        public Type GetGeneratingType()
        {
            return typeof(bool);
        }

        //public bool GetTypeRandomValue()
        //{
        //    var random = new Random();
        //    byte[] bytesBuffer = new byte[8];
        //    random.NextBytes(bytesBuffer);

        //    return BitConverter.ToBoolean(bytesBuffer, 7);
        //}
    }
}
