﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakerConsole.GeneratorPlugins
{
    public class CustomTypeRandomGenerator
    {
        public List<IRandomValueGenerator> generators = new List<IRandomValueGenerator>();

        public CustomTypeRandomGenerator()
        {

        }

        public void AddGenerator(IRandomValueGenerator generator)
        {
            generators.Add(generator);
        }

        public object Generate(Type type)
        {
            foreach(IRandomValueGenerator suitableGenerator in generators)
            {
                if (suitableGenerator.GetGeneratingType() == type)
                {
                    return suitableGenerator.Generate(type);
                }
            }
            return null;
        }
    }
}
