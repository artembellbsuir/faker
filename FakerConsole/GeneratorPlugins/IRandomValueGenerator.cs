﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakerConsole
{
    public interface IRandomValueGenerator
    {
        dynamic Generate(Type type);

        //T Generate(T)

        Type GetGeneratingType();

        //dynamic GetTypeRandomValue(Type type);
    }
}
