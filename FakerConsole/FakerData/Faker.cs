﻿using FakerConsole.GeneratorPlugins;
using FakerConsole.Tests;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FakerConsole.FakerData
{
    public class Faker
    {
        private static Random random = new Random();
        private readonly DiagnosticTree tree = null;

        //private IStringGenerator stringValueGenerator = null;
        //private IBooleanGenerator booleanValueGenerator = null;

        private CustomTypeRandomGenerator generator;

        public Faker()
        {
            tree = new DiagnosticTree();
        }

        /*
         * Loads third party random value generators from .dll
         */
        public void LoadGenerators()
        {
            Assembly assembly = Assembly.LoadFrom("ValueGenerators.dll");

            Type[] assemblyTypes = assembly.GetTypes();
            generator = new CustomTypeRandomGenerator();

            foreach (Type type in assemblyTypes)
            {
                generator.AddGenerator((IRandomValueGenerator)Activator.CreateInstance(type));
                //if (type.GetInterface(typeof(IStringGenerator).Name) != null)
                //{
                //    stringValueGenerator = (IStringGenerator)Activator.CreateInstance(type);
                //}
                //else if (type.GetInterface(typeof(IBooleanGenerator).Name) != null)
                //{
                //    booleanValueGenerator = (IBooleanGenerator)Activator.CreateInstance(type);
                //}
            }
        }

        /*
         * Checks if type should be treated as DTO
         */
        internal bool IsDtoDerived(Type type)
        {
            var derived = type;

            while (derived != null)
            {
                derived = derived.BaseType;
                if (derived == typeof(DTO))
                {
                    return true;
                }
            }
            return false;
        }

        /*
         * Casts List<object> to List<targetType>
         */
        //internal object CastListElements(List<object> list, Type targetType)
        //{
        //    if (targetType == typeof(int)) return list.Cast<int>().ToList();
        //    if (targetType == typeof(long)) return list.Cast<long>().ToList();
        //    if (targetType == typeof(float)) return list.Cast<float>().ToList();
        //    if (targetType == typeof(double)) return list.Cast<double>().ToList();
        //    if (targetType == typeof(bool)) return list.Cast<bool>().ToList();
        //    if (targetType == typeof(string)) return list.Cast<string>().ToList();
        //    if (targetType == typeof(DateTime)) return list.Cast<DateTime>().ToList();
        //    return null;
        //}

        /*
         * Generates random values for specified type
         */
        internal object GenerateTypeRandomValue(Type type)
        {
            //var random = new Random();
            byte[] bytesBuffer = new byte[8];
            random.NextBytes(bytesBuffer);

            // check if object is DTO
            if (IsDtoDerived(type))
            {
                // check if encountering circular loop
                if (tree.current.HasTopLevelType(type))
                {
                    throw new CircularLoopException("Circular loop was detected.");
                }

                var newDto = CreateDto(type);
                tree.current = tree.current.parent;
                return newDto;
            }
            else
            {
                if (type == typeof(int)) return BitConverter.ToInt32(bytesBuffer, 4);
                if (type == typeof(long)) return BitConverter.ToInt64(bytesBuffer, 0);
                if (type == typeof(float)) return BitConverter.ToSingle(bytesBuffer, 4);
                if (type == typeof(double)) return BitConverter.ToDouble(bytesBuffer, 0);

                if (type == typeof(bool)) return generator.Generate(type);
                //if (type == typeof(string)) return BitConverter.ToString(bytesBuffer);
                if (type == typeof(DateTime))
                {
                    var minDateTime = new DateTime(2000, 1, 1, 0, 0, 0);
                    return minDateTime.AddYears(random.Next(0, 20))
                        .AddMonths(random.Next(0, 11))
                        .AddDays(random.Next(0, 28))
                        .AddHours(random.Next(0, 23))
                        .AddMinutes(random.Next(0, 59))
                        .AddSeconds(random.Next(0, 59));
                }
                if (type.GetInterfaces().Contains(typeof(IList)))
                {
                    return GenerateList(type);
                }

                //return generator.Generate(type);
                return default(Type);
            }
        }

        internal IList GenerateList(Type type)
        {
            int listLength = random.Next(1, 10);

            Type[] genericTypeParameters = type.GetGenericArguments();
            if (genericTypeParameters.Length == 1)
            {
                Type baseType = genericTypeParameters[0];
                //var list = new List<object>();
                var list = (IList)Activator.CreateInstance(type);

                for (int i = 0; i < listLength; i++)
                {
                    var val = GenerateTypeRandomValue(baseType);
                    list.Add(val);
                }
                return list;
            }
            return null;
        }

        /*
         * Generates parameters list for constructor
         */
        internal IList<Object> GetDefaultParametersList(ConstructorInfo constructorInfo)
        {
            var generatedParametersList = new List<Object>();
            foreach (ParameterInfo parameterInfo in constructorInfo.GetParameters())
            {
                generatedParametersList.Add(GenerateTypeRandomValue(parameterInfo.ParameterType));
            }

            return generatedParametersList;
        }

        /*
         * Sets public | instance with public setter dto props
         */
        internal void SetProperties(Object dto)
        {
            foreach (PropertyInfo propertyInfo in dto.GetType().GetProperties(
                BindingFlags.Public))
            {
                // if prop has public setter
                if (propertyInfo.GetSetMethod() != null)
                {
                    propertyInfo.SetValue(dto, GenerateTypeRandomValue(propertyInfo.PropertyType));
                }
            }
        }

        /*
         * Sets public | instance not readonly dto fields
         */
        internal void SetFields(Object dto)
        {
            foreach (FieldInfo fieldInfo in dto.GetType().GetFields(
                BindingFlags.Public | BindingFlags.Instance))
            {
                // if field is not readonly (can be initialized ONLY in constructor)
                if (!fieldInfo.IsInitOnly)
                {
                    fieldInfo.SetValue(dto, GenerateTypeRandomValue(fieldInfo.FieldType));
                }
            }
        }

        /*
         * Creates DTO object by constructor with maximum number of parameters
         */
        internal DTO CreateWithConstructor(Type type)
        {
            ConstructorInfo bestConstructor = null;
            int maxParameters = -1;

            foreach (ConstructorInfo consturctorInfo in type.GetConstructors())
            {
                if (consturctorInfo.GetParameters().Length > maxParameters)
                {
                    maxParameters = consturctorInfo.GetParameters().Length;
                    bestConstructor = consturctorInfo;
                }
            }

            // there's always a default constructor without params
            return (DTO)bestConstructor.Invoke(GetDefaultParametersList(bestConstructor).ToArray());
        }

        /*
         * Creates DTO (for nested DTOs - internal usage)
         */
        internal DTO CreateDto(Type dtoType)
        {
            if (IsDtoDerived(dtoType))
            {
                Console.WriteLine(dtoType);

                var parent = tree.current;
                var newNode = parent.Add(new Node(dtoType));
                newNode.parent = parent;
                tree.current = newNode;

                var dto = CreateWithConstructor(dtoType);
                SetProperties(dto);
                SetFields(dto);

                return dto;
            }

            return null;
        }

        /*
         * Creates DTO (main method - public usage)
         */
        public DTO Create<ExactDtoType>()
        {
            try
            {
                Type type = typeof(ExactDtoType);
                return CreateDto(type);
            }
            catch (CircularLoopException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
