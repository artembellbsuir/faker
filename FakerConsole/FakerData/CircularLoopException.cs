﻿using System;

namespace FakerConsole.FakerData
{
    public class CircularLoopException : Exception
    {
        public CircularLoopException(string message) : base(message) { }
    }
}
