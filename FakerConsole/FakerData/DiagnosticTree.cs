﻿using System;
using System.Collections.Generic;

namespace FakerConsole.FakerData
{
    public class DiagnosticTree
    {
        public Node root { get; } = null;
        public Node current { get; set; } = null;

        public DiagnosticTree()
        {
            root = new Node(null);
            current = root;
        }

    }

    public class Node
    {
        public Type type { get; set; } = null;
        public Node parent { get; set; } = null;
        public List<Node> children { get; set; }

        public Node(Type type)
        {
            this.type = type;
            children = new List<Node>();
        }

        public Node Add(Node node)
        {
            children.Add(node);
            return node;
        }

        public bool HasTopLevelType(Type type)
        {
            Node currentNode = this;
            while (currentNode.type != null)
            {
                if (currentNode.type == type)
                {
                    return true;
                }
                currentNode = currentNode.parent;
            }

            return false;
        }
    }
}
