﻿namespace FakerConsole.Tests
{
    public class Top : DTO
    {
        public A a { get; set; }
    }

    public class A : DTO
    {
        public B b { get; set; }
    }

    public class B : DTO
    {
        public C c { get; set; }
        //public A a { get; set; }
    }

    public class C : DTO
    {
        //public A a { get; set; }
    }
}
