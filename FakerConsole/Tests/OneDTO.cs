﻿using System;
using System.Collections.Generic;

namespace FakerConsole.Tests
{
    public class DTO { }

    public class NotDTO
    {
        public int notDtoField;
    }

    public class TwoDTO : DTO
    {
        public string exampleString;
    }

    public class OneDTO : DTO
    {
        private string privateField = default;
        public int publicField = default;
        public readonly double publicReadonlyField = default;

        public int Fint = default;
        public long Flong = default;
        public float Ffloat = default;
        public double Fdouble = default;
        public bool Fbool = default;
        public string Fstring = default;

        public NotDTO notDtoObject = default;
        public TwoDTO dtoObject = default;
        public DateTime dateTime = default;
        public List<string> stringList = default;

        public int publicPropertyWithPublicSetter { get; set; }
        public int publicPropertyWithPrivateSetter { get; private set; }

        public OneDTO(string a, int b, double c)
        {
            privateField = a;
            publicField = b;
            publicReadonlyField = c;
        }

        public OneDTO(string a, int b)
        {
            privateField = a;
            publicField = b;
        }
    }
}
